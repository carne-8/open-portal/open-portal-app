namespace App

open Feliz.ReactNative

[<RequireQualifiedAccess>]
type Msg =
    | SetLoading of bool
    | ThemeChanged of ColorScheme
    | ErrorOccurred of string

type Model =
    { Theme: ColorScheme
      Error: string option
      Loading: bool }