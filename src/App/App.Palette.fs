module App.Palette

open Feliz.ReactNative.MaterialYou

let defaultPalette: Palette =
    { system_accent1 =
        [| "#ffffff"
           "#fcfcfc"
           "#ffede4"
           "#ffdbc9"
           "#ffb690"
           "#ff8c4c"
           "#e17233"
           "#bf591c"
           "#a04200"
           "#7a2f00"
           "#561f00"
           "#351000"
           "#000000" |]
      system_accent2 =
        [| "#ffffff"
           "#fcfcfc"
           "#ffede4"
           "#ffdbc9"
           "#e6beac"
           "#c9a392"
           "#ad8978"
           "#917060"
           "#765748"
           "#5c4032"
           "#432a1e"
           "#2b160b"
           "#000000" |]
      system_accent3 =
        [| "#ffffff"
           "#fffbf7"
           "#fbf2b7"
           "#ece4aa"
           "#d0c890"
           "#b4ad77"
           "#99925e"
           "#7e7847"
           "#656032"
           "#4d481c"
           "#353107"
           "#1f1c00"
           "#000000" |]
      system_neutral1 =
        [| "#ffffff"
           "#fcfcfc"
           "#fbeee9"
           "#ede0db"
           "#d0c4bf"
           "#b4a9a5"
           "#998f8b"
           "#7f7571"
           "#655c59"
           "#4d4542"
           "#362f2c"
           "#201a17"
           "#000000" |]
      system_neutral2 =
        [| "#ffffff"
           "#fcfcfc"
           "#ffede4"
           "#f4ded5"
           "#d7c2b9"
           "#bba79f"
           "#a08d85"
           "#85746c"
           "#6b5b54"
           "#53443d"
           "#3b2d27"
           "#241914"
           "#000000" |] }
