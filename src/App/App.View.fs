module App

open App

open Feliz
open Feliz.ReactNative
open Feliz.ReactNative.WebView
open Feliz.ReactNative.MaterialYou

open Fable.Core.JsInterop

let generateJS (theme: ColorScheme) (palette: Palette) =
    let colors =
        palette
        |> Palette.paletteToDict theme

    colors
    |> Seq.map (fun color ->
        sprintf "document.body.style.setProperty(\"--color-%s\", \"%s\")" color.Key color.Value
    )
    |> String.concat ";"


[<ReactComponent>]
let WebView model (dispatch: Elmish.Dispatch<App.Msg>) =
    let webviewRef = React.useElementRef()

    let palette =
        if MaterialYou.deviceSupportsMaterialYou()
        then MaterialYou.useMaterialYouPalette()
        else Palette.defaultPalette

    React.useEffect (fun _ ->
        match webviewRef.current with
        | None -> ()
        | Some webviewRef ->
            palette
            |> generateJS model.Theme
            |> webviewRef?injectJavaScript
    )

    React.fragment [
        Comp.statusBar [
            prop.backgroundColor (Palette.findInPalette "background" model.Theme palette)

            if model.Theme = Dark
            then prop.statusBarStyle.lightContent
            else prop.statusBarStyle.darkContent
        ]

        Comp.webView [
            if model.Error |> Option.isSome || model.Loading then
                prop.containerStyle [ style.display.none ]
            prop.ref webviewRef
            prop.injectedJavaScript (generateJS model.Theme palette)
            prop.webViewSource [ WebViewSource.uri "https://carne8-open-portal.netlify.app" ]
            prop.onLoadStart (fun _ -> true |> Msg.SetLoading |> dispatch)
            prop.onLoadEnd (fun _ -> false |> Msg.SetLoading |> dispatch)
            prop.onError (fun x ->
                x.nativeEvent.description
                |> Msg.ErrorOccurred
                |> dispatch
            )
            prop.onHttpError (fun x ->
                x.nativeEvent.statusCode
                |> Msg.ErrorOccurred
                |> dispatch
            )
        ]

        if model.Error |> Option.isSome then
            Components.Error.View
                model.Theme
                palette
                (fun _ ->
                    match webviewRef.current with
                    | None -> ()
                    | Some webviewRef -> webviewRef?reload()
                )
                model.Error.Value

        if model.Loading then
            Components.Loading.View model.Theme palette
    ]

[<ReactComponent>]
let View model dispatch =
    Comp.MaterialYouService [
        prop.fallbackPalette Palette.defaultPalette
        prop.children [ WebView model dispatch ]
    ]