namespace App

open Elmish
open Feliz.ReactNative

module Cmds =
    let appearanceListener () =
        Cmd.ofSub (fun dispatch ->
            ReactNative.appearance.addChangeListener (fun x ->
                x.colorScheme
                |> Msg.ThemeChanged
                |> dispatch
            )
            |> ignore
        )

module State =
    let init () =
        let theme = ReactNative.appearance.getColorScheme()

        { Theme = theme
          Error = None
          Loading = true }, Cmds.appearanceListener()

    let update msg model =
        match msg with
        | Msg.SetLoading loading ->
            { model with Loading = loading }, Cmd.none
        | Msg.ThemeChanged newTheme ->
            { model with Theme = newTheme }, Cmd.none
        | Msg.ErrorOccurred error ->
            { model with Error = error |> Some }, Cmd.none