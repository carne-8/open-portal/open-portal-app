namespace Feliz.ReactNative

open Feliz
open Fable.Core
open Fable.Core.JsInterop

[<Erase>]
[<StringEnum>]
type ColorScheme =
    | Light
    | Dark

[<Erase>]
type ReactNative =
    static member useColorScheme () : ColorScheme = import "useColorScheme" "react-native"
    static member useWindowDimensions () : {| width: float; height: float; fontScale: float; scale: float |} = import "useWindowDimensions" "react-native"

[<Erase>]
module ReactNative =
    [<Erase>]
    type appearance =
        static member inline getColorScheme () : ColorScheme = (import "Appearance" "react-native")?getColorScheme()
        static member inline addChangeListener (value: {| colorScheme: ColorScheme |} -> unit) : {| remove: unit -> unit |} = (import "Appearance" "react-native")?addChangeListener value