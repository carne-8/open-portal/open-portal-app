namespace Feliz.ReactNative.WebView

open Fable.Core

type IWebViewSource = interface end

[<Erase>]
type WebViewSource =
    static member inline uri (value: string) = unbox<IWebViewSource> ("uri", value)
    static member inline method (value: string) = unbox<IWebViewSource> ("method", value)