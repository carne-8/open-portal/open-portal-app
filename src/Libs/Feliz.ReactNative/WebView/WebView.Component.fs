namespace Feliz.ReactNative.WebView

open Feliz
open Feliz.ReactNative
open Fable.Core
open Fable.Core.JsInterop

[<Erase>]
type Comp =
    static member inline webView (props: seq<IReactProperty>) = Interop.nativeReactElement (import "WebView" "react-native-webview") (createObj !!props)