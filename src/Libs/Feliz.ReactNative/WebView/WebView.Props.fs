namespace Feliz.ReactNative.WebView

open Feliz
open Fable.Core
open Fable.Core.JsInterop
open Fable.React

[<Erase>]
type prop =
    static member inline containerStyle (value: seq<IStyleAttribute>) = Interop.mkAttr "containerStyle" (createObj !!value)
    static member inline webViewSource (value: seq<IWebViewSource>) = Interop.mkAttr "source" (createObj !!value)
    static member inline injectedJavaScript (value: string) = Interop.mkAttr "injectedJavaScript" value
    static member inline injectedJavaScriptBeforeContentLoaded (value: string) = Interop.mkAttr "injectedJavaScriptBeforeContentLoaded" value
    static member inline originWhitelist (value: string list) = Interop.mkAttr "originWhitelist" value
    static member inline allowingReadAccessToURL (value: string) = Interop.mkAttr "allowingReadAccessToURL" value

    static member inline onLoadStart (value: unit -> unit) = Interop.mkAttr "onLoadStart" value
    static member inline onLoadEnd (value: unit -> unit) = Interop.mkAttr "onLoadEnd" value
    static member inline onHttpError (element: {| nativeEvent: {| statusCode: string |} |} -> unit) = Interop.mkAttr "onHttpError" element
    static member inline onError (element: {| nativeEvent: {| description: string |} |} -> unit) = Interop.mkAttr "onError" element