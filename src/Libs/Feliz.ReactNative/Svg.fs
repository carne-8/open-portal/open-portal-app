namespace Feliz.ReactNative.Svg

open Fable.Core
open Fable.Core.JsInterop
open Feliz
open Feliz.ReactNative

[<Erase>]
type Comp =
    static member inline svg source (props: IReactProperty list) = Interop.nativeReactElement source (createObj !!props)