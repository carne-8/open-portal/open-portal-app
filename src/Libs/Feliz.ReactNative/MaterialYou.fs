namespace Feliz.ReactNative.MaterialYou

open Fable.Core
open Fable.Core.JsInterop
open Feliz
open Feliz.ReactNative

type Palette =
    { system_accent1: string []
      system_accent2: string []
      system_accent3: string []
      system_neutral1: string []
      system_neutral2: string [] }

module Palette =
    let inline paletteToDict theme (rawPalette: Palette) =
        match theme with
        | Light ->
            [ "primary", rawPalette.system_accent1.[8]
              "on-primary", rawPalette.system_accent1.[0]
              "primary-container", rawPalette.system_accent1.[3]
              "on-primary-container", rawPalette.system_accent1.[11]

              "secondary", rawPalette.system_accent2.[8]
              "on-secondary", rawPalette.system_accent2.[0]
              "secondary-container", rawPalette.system_accent2.[3]
              "on-secondary-container", rawPalette.system_accent2.[11]

              "tertiary", rawPalette.system_accent3.[8]
              "on-tertiary", rawPalette.system_accent3.[0]
              "tertiary-container", rawPalette.system_accent3.[3]
              "on-tertiary-container", rawPalette.system_accent3.[11]

              "background", rawPalette.system_neutral1.[1]
              "on-background", rawPalette.system_neutral1.[11]

              "surface", rawPalette.system_neutral1.[1]
              "on-surface", rawPalette.system_neutral1.[11]

              "surface-variant", rawPalette.system_neutral2.[3]
              "on-surface-variant", rawPalette.system_neutral2.[9]

              "outline", rawPalette.system_neutral2.[7]

              "surface-4", rawPalette.system_accent1.[2] ]
        | Dark ->
            [ "primary", rawPalette.system_accent1.[4]
              "on-primary", rawPalette.system_accent1.[10]
              "primary-container", rawPalette.system_accent1.[9]
              "on-primary-container", rawPalette.system_accent1.[3]

              "secondary", rawPalette.system_accent2.[4]
              "on-secondary", rawPalette.system_accent2.[10]
              "secondary-container", rawPalette.system_accent2.[9]
              "on-secondary-container", rawPalette.system_accent2.[3]

              "tertiary", rawPalette.system_accent3.[4]
              "on-tertiary", rawPalette.system_accent3.[10]
              "tertiary-container", rawPalette.system_accent3.[9]
              "on-tertiary-container", rawPalette.system_accent3.[3]

              "background", rawPalette.system_neutral1.[11]
              "on-background", rawPalette.system_neutral1.[3]

              "surface", rawPalette.system_neutral1.[11]
              "on-surface", rawPalette.system_neutral1.[4]

              "surface-variant", rawPalette.system_neutral2.[9]
              "on-surface-variant", rawPalette.system_neutral2.[4]

              "outline", rawPalette.system_neutral2.[6]

              "surface-4", rawPalette.system_neutral2.[10] ]
        |> dict

    let findInPalette colorKey theme palette =
        palette
        |> paletteToDict theme
        |> Seq.find (fun x -> x.Key = colorKey)
        |> fun x -> x.Value

[<Erase>]
type MaterialYou =
    static member inline defaultPalette: Palette = import "defaultPalette" "@assembless/react-native-material-you"
    static member inline deviceSupportsMaterialYou () = import "deviceSupportsMaterialYou" "@assembless/react-native-material-you"

    static member inline useMaterialYou (fallbackPalette: Palette) : {| palette: Palette; isSupported: bool; _refresh: unit -> unit |} = import "useMaterialYou" "@assembless/react-native-material-you" fallbackPalette
    static member inline useMaterialYouPalette () : Palette = import "useMaterialYouPalette" "@assembless/react-native-material-you"
    static member inline getPalette () : Palette = import "getPalette" "@assembless/react-native-material-you"
    static member inline getPaletteSync () : Palette = import "getPaletteSync" "@assembless/react-native-material-you"


[<Erase>]
type prop =
    static member inline fallbackPalette (value: Palette) = Interop.mkAttr "fallbackPalette" value

[<Erase>]
type Comp =
    static member inline MaterialYouService (props: seq<IReactProperty>) = Interop.nativeReactElement (import "MaterialYouService" "@assembless/react-native-material-you") (createObj !!props)