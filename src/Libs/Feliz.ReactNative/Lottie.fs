namespace Feliz.ReactNative.Lottie

open Fable.Core
open Fable.Core.JsInterop
open Feliz
open Feliz.ReactNative
open Fable.React
open Feliz

type ILottieProperty = interface end
type ILottieSource = interface end
type ILottieSourceProperty = interface end

[<Erase>]
type lottieSource =
    static member inline uri (uri: string) = unbox<ILottieSourceProperty> ("uri", uri)
    static member inline local (path: string) = unbox<ILottieSource> (importAll path)

type ColorFilter =
    { keypath: string
      color: string }

[<Erase>]
type lottieProp =
    static member inline ref (ref: IRefValue<_>) = unbox<ILottieProperty> ("ref", ref)
    static member inline ref (ref: ReactElement -> unit) = unbox<ILottieProperty> ("ref", ref)
    static member inline source (source: ILottieSource) = unbox<ILottieProperty> ("source", source)
    static member inline source (source: seq<ILottieSourceProperty>) = unbox<ILottieProperty> ("source", (createObj !!source))
    static member inline style (style: seq<IStyleAttribute>) = unbox<ILottieProperty> ("style", (createObj !!style))

    static member inline loop (value: bool) = unbox<ILottieProperty> ("loop", value)
    static member inline autoPlay (value: bool) = unbox<ILottieProperty> ("autoPlay", value)
    static member inline autoSize (value: bool) = unbox<ILottieProperty> ("autoSize", value)
    static member inline colorFilters (value: seq<ColorFilter>) = unbox<ILottieProperty> ("colorFilters", value)

[<Erase>]
module lottieProp =
    [<Erase>]
    type resizeMode =
        static member inline cover = unbox<ILottieProperty> ("resizeMode", "cover")
        static member inline contain = unbox<ILottieProperty> ("resizeMode", "contain")
        static member inline center = unbox<ILottieProperty> ("resizeMode", "center")

[<Erase>]
type Comp =
    static member inline lottie (props: seq<ILottieProperty>) = Interop.nativeReactElement (importDefault "lottie-react-native") (createObj !!props)