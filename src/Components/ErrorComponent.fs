module Components.Error

open Feliz.ReactNative
open Feliz.ReactNative.Svg
open Feliz.ReactNative.MaterialYou

open Fable.Core.JsInterop

[<Feliz.ReactComponent>]
let ErrorIcon theme palette =
    Comp.svg (importDefault "../../assets/icons/information.svg") [
        prop.style [
            style.justifyContent.center
            style.alignItems.center
            style.color (Palette.findInPalette "on-surface" theme palette)
        ]
    ]

[<Feliz.ReactComponent>]
let View theme palette onPress error =
    let color colorName =
        Palette.findInPalette colorName theme palette

    Comp.view [ // Container
        prop.style [
            style.position.absolute
            style.top 0
            style.left 0
            style.right 0
            style.bottom 0
            style.backgroundColor "#00000033"

            style.justifyContent.center
            style.alignItems.center
        ]
        prop.children [

            Comp.view [ // Dialog
                prop.style [
                    style.backgroundColor (color "surface")

                    style.minWidth 280
                    style.maxWidth 560

                    style.alignItems.center

                    style.padding 24
                    style.borderRadius 28
                    style.elevation 10
                ]
                prop.children [

                    ErrorIcon theme palette

                    Comp.text [
                        prop.style [
                            style.fontFamily "Roboto-Regular"
                            style.fontSize 24
                            style.lineHeight 32
                            style.fontWeight.``400``
                            style.color (color "on-surface")

                            style.marginVertical 16
                        ]
                        prop.text "An error occurred."
                    ]

                    Comp.text [
                        prop.style [
                            style.fontFamily "Roboto-Medium"
                            style.fontSize 14
                            style.lineHeight 20
                            style.fontWeight.``400``
                            style.color (color "on-surface-variant")

                            style.textAlign.left

                            style.marginBottom 24
                        ]
                        prop.text (sprintf "There was an error when the website was loading:\n%s" error)
                    ]

                    Comp.view [
                        prop.style [
                            style.alignSelf.flexEnd
                            style.alignItems.center
                        ]
                        prop.children [
                            MaterialDesign.TextButton
                                {| Theme = theme
                                   Palette = palette
                                   Text = "Reload"
                                   OnPress = onPress |}
                        ]
                    ]
                ]
            ]
        ]
    ]