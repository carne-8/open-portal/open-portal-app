module App.Components.Loading

open Feliz
open Feliz.ReactNative
open Feliz.ReactNative.Lottie
open Feliz.ReactNative.MaterialYou

[<ReactComponent>]
let View theme (palette: Palette) =
    let window = ReactNative.useWindowDimensions()

    let color colorName =
        Palette.findInPalette colorName theme palette

    Comp.view [
        prop.style [
            style.width "100%"
            style.height "100%"

            style.backgroundColor (color "background")

            style.justifyContent.center
            style.alignItems.center
        ]
        prop.children [
            Comp.lottie [
                lottieProp.autoPlay true
                lottieProp.style [
                    style.width 250
                ]
                lottieProp.colorFilters [
                    { keypath = "icon"
                      color = color "primary" }
                    { keypath = "icon 2"
                      color = color "primary" }
                ]
                lottieProp.source (lottieSource.local "../../assets/loading-animation.json")
            ]

            Comp.text [
                prop.style [
                    style.fontFamily "Roboto-Bold"
                    style.textAlign.center
                    style.fontSize 16
                    style.color (color "on-background")
                ]
                prop.text "Loading"
            ]
        ]
    ]