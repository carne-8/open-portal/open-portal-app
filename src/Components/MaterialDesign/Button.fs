module Components.MaterialDesign

open Feliz.ReactNative
open Feliz.ReactNative.MaterialYou

[<Feliz.ReactComponent>]
let TextButton (props: {| Theme: ColorScheme; Palette: Palette; Text: string; OnPress: unit -> unit |}) =
    Comp.pressable [
        prop.style [
            style.height 40
            style.minWidth 48
            style.paddingHorizontal 12
            style.borderRadius 100

            style.alignItems.center
            style.justifyContent.center
        ]
        prop.hitSlop 10
        prop.onPress (fun _ -> props.OnPress())
        prop.children [
            Comp.text [
                prop.style [
                    style.fontFamily "Roboto-Medium"
                    style.fontSize 14
                    style.lineHeight 20
                    style.fontWeight.``500``

                    style.color (Palette.findInPalette "primary" props.Theme props.Palette)
                ]
                prop.text props.Text
            ]
        ]
    ]