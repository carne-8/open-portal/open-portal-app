module Main

open Elmish
open Elmish.React
open Elmish.ReactNative
open Elmish.Debug

open Fable.Core

#if DEBUG
[<Emit "typeof atob !== 'undefined'">]
let isRemoteDebugging: bool = jsNative
#else
let isRemoteDebugging = false
#endif

Program.mkProgram App.State.init App.State.update App.View
|> Program.withReactNative "open_portal"
|> if isRemoteDebugging then Program.withDebugger else id
|> Program.run